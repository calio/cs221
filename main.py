import simulator as sim
import numpy as np
import pickle


def random_policy(env, state):
    return env.action_space.sample()

def first_fit(env, state):
    cpu, mem, disk = state[0], state[1], state[2]
    cpu_total = env.cluster.nodes[0].total_cpu
    mem_total = env.cluster.nodes[0].total_mem
    disk_total = env.cluster.nodes[0].total_disk

    nodes = env.nodes
    cpu_score = [(state[3 + i]-cpu)/cpu_total for i in range(env.nodes)]
    mem_score = [(state[3 + nodes + i]-mem)/mem_total for i in range(env.nodes)]
    disk_score = [(state[3 + 2*nodes + i]-disk)/disk_total for i in range(env.nodes)]

    first_i = nodes - 1
    #print(cpu_score, mem_score, disk_score)
    for i in range(env.nodes):
        if cpu_score[i] >= 0 and mem_score[i] >= 0 and disk_score[i]>=0:
            first_i = i
            break
    return first_i

def best_fit(env, state):
    cpu, mem, disk = state[0], state[1], state[2]
    cpu_total = env.cluster.nodes[0].total_cpu
    mem_total = env.cluster.nodes[0].total_mem
    disk_total = env.cluster.nodes[0].total_disk

    nodes = env.nodes
    cpu_score = [(state[3 + i]-cpu)/cpu_total for i in range(env.nodes)]
    mem_score = [(state[3 + nodes + i]-mem)/mem_total for i in range(env.nodes)]
    disk_score = [(state[3 + 2*nodes + i]-disk)/disk_total for i in range(env.nodes)]

    best_i = 0
    best_score = 0
    best_fit_i = -1
    best_fit_score = 0
    #print(cpu_score, mem_score, disk_score)
    for i in range(env.nodes):
        score = cpu_score[i] + mem_score[i] + disk_score[i]
        print('cpu_score', cpu_score[i], 'mem_score', mem_score[i], 'disk_score', disk_score[i], 'score', score, 'best_score', best_score)
        if score > best_score:
            best_i = i
            best_score = score
        if cpu_score[i] >= 0 and mem_score[i] >= 0 and disk_score[i]>=0 and score> best_fit_score:
            best_fit_i = i
            best_fit_score = score
    print("best_i", best_i, "best_fit_i", best_fit_i)

    return best_fit_i if best_fit_i != -1 else best_i


def run():
    #play_back_file = "exp-4-14.pkl"
    play_back_file = None

    #policy = "random"
    #policy = "best_fit"
    policy = "first_fit"
    #policy = "fixed"

    if play_back_file is not None:
        workload, fixed_actions, cost = sim.read_play_back_file(play_back_file)
        nodes = 4
        n = len(fixed_actions)
    else:
        workload = None
        nodes = 4
    #env = sim.GymEnvironment()
    env = sim.GymEnvironmentTimeReward(
            nodes=nodes,
            num_tasks=800,
            workload=workload)
    state = env.reset()
    rewards = []
    i = 0
    while True:
    #for _ in range(1000):
        env.render()
        if policy == "random":
            action = random_policy(env, state)
        elif policy == "best_fit":
            action = best_fit(env, state)
        elif policy == "first_fit":
            action = first_fit(env, state)
        elif policy == "fixed":
            action = fixed_actions[i % n]
        i += 1

        print('action', action)
        state, reward, done, _ = env.step(action) # take a random action
        print("reward", reward, "done", done)
        rewards.append(reward)
        if done:
            break
    env.close()
    total_rewards = sum(rewards)
    print("total reward", total_rewards)
    print("rewards", rewards)
    return total_rewards


def main():
    rewards = []
    n = 100
    for i in range(n):
        rewards.append(run())

    print("mean total rewards", np.mean(rewards))

if __name__ == '__main__':
    main()

