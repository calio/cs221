import heapq
import pickle
import gym
import copy
import numpy as np
from gym import spaces


class Task(object):
    def __init__(self, duration, start, cpu, mem, disk):
        self.cpu = cpu
        self.mem = mem
        self.disk = disk
        self.duration = duration
        self.orig_duration = duration
        self.start = start
        self.placement = None

    def __lt__(self, other):
        if self.placement is None and other.placement is None:
            return (self.start + self.orig_duration) < (other.start + other.orig_duration)
        elif self.placement is not None and other.placement is not None:
            return self.duration < other.duration
        else:
            raise ValueError("can't compare started and not start task")
#        if self.start != other.start:
#            return self.start < other.start
#        if self.duration != other.duration:
#            return self.duration < other.duration
#        if self.cpu != other.cpu:
#            return self.cpu < other.cpu
#        if self.mem != other.mem:
#            return self.mem < other.mem
#        return self.disk < other.disk

    def __eq__(self, other):
        return (self.start == other.start and
                self.duration == other.duration and
                self.cpu == other.cpu and
                self.mem == other.mem and
                self.disk == other.disk)

    def __repr__(self):
        return "(s:{},D:{},oD:{},c:{},m:{},d:{},P:{})".format(self.start, self.duration, self.orig_duration, self.cpu, self.mem, self.disk, self.placement)


class Machine(object):
    def __init__(self, cpu, mem, disk):
        self.cpu = cpu
        self.mem = mem
        self.disk = disk

        self.total_cpu = self.cpu
        self.total_mem = mem
        self.total_disk = disk

    def __repr__(self):
        return "Mach<c:{},m:{},d:{}>".format(self.cpu, self.mem, self.disk)

    def __eq__(self, other):
        return self.cpu == other.cpu and self.mem == other.mem and self.disk == other.disk

    def run(self, task):
        if task.cpu > self.cpu:
            return False
        if task.mem > self.mem:
            return False
        if task.disk > self.disk:
            return False

        self.cpu -= task.cpu
        self.mem -= task.mem
        self.disk -= task.disk

        return True

    def finish(self, task):
        self.cpu += task.cpu
        self.mem += task.mem
        self.disk += task.disk


class Cluster(object):
    def __init__(self, num_nodes, cpu, mem, disk):
        self.nodes = [Machine(cpu, mem, disk) for i in range(num_nodes)]
        self.task_queue = []
        self.running_queue = []
        self.clock = 0

    def __repr__(self):
        return "Cluster<({}), nodes:{}, RUN_Q:{}>".format(self.clock, str(self.nodes), str(self.running_queue))

    def to_tuple(self):
        N = len(self.nodes)
        return tuple((self.nodes[i].cpu, self.nodes[i].mem, self.nodes[i].disk) for i in range(N))

    def from_tuple(self, tuple_data):
        self.nodes = [Machine(d[0], d[1], d[2]) for d in tuple_data]

    def do_schedule(self, task, placement):
        succ = self.nodes[placement].run(task)
        if not succ:
            return False
        task.placement = placement
        heapq.heappush(self.running_queue, task)
        return True

    def schedule(self, task, placement):
        ts = self.clock
        # forward to task start time if necessary
        if self.clock < task.start:
            self.forward(task.start)
            #print("[schedule]forward to ", task.start, self)

        # try to run task
        succ = self.do_schedule(task, placement)
        while not succ:
            # task not run due to not enough resource:
            t = self.tick()
            succ = self.do_schedule(task, placement)

        #print("[after schedule]", self)
        return self.clock - ts

    def forward(self, ts):
        # forward to time ts
        old_clock = self.clock
        self.clock = ts
        elapsed = ts - old_clock
        if len(self.running_queue) == 0:
            return

        # process finished tasks
        while len(self.running_queue) > 0:
            # peek
            task = self.running_queue[0]
            if old_clock + task.duration <= self.clock:
                task = heapq.heappop(self.running_queue)
                self.nodes[task.placement].finish(task)
            else:
                break
        # process not-finished tasks
        for task in self.running_queue:
            task.duration -= elapsed

        return

    def tick(self):
        #print("pre tick", self.running_queue)
        #print("pre tick", self)
        old_clock = self.clock
        # run until one task finish
        if len(self.running_queue) == 0:
            return 0

        # get a that will finish the most recent
        task = heapq.heappop(self.running_queue)
        self.nodes[task.placement].finish(task)

        duration = task.duration
        task.duration = 0
        #print("duration", duration)
        for task in self.running_queue:
            #print("task and duration", task, duration)
            task.duration -= duration

        #duration = task.duration
        self.clock += duration
        #print("duration, ", duration)
        #duration = tend - self.clock

        #print("after tick", self.running_queue)
        # return duration from last tick to now
        #if duration >= 0:
            #self.clock = tend
        #print("after tick", self)
        return duration
        #else:
        #    return 0

    def run(self, task, placement):
        # fast forward to now
        reward = 0
        finished = 0

        if task is None:
            # no more tasks
            if len(self.running_queue) == 0:
                return True, 0
            else:
                elapsed = self.running_queue[-1].start + self.running_queue[-1].orig_duration - self.clock
        else:
            if self.clock < task.start:
                elapsed = task.start - self.clock
            else:
                elapsed = 0
        self.clock += elapsed

        if elapsed != 0:
            for t in self.running_queue:
                t.duration -= elapsed
            while True:
                if len(self.running_queue) == 0:
                    break
                t = self.running_queue[0]
                if t.duration <= 0:
                    #print("task finished", t)
                    t = heapq.heappop(self.running_queue)
                    reward += t.orig_duration
                    finished += 1
                    self.nodes[t.placement].finish(t)
                else:
                    break

        #print("run task", task, "with placement", placement)
        if task is None:
            return True, float(reward) / finished if finished != 0 else 0

        task.placement = placement
        r = self.nodes[placement].run(task)
        if r is True:
            heapq.heappush(self.running_queue, task)
        else:
            # invalid placement
            #print("invalid placement", placement)
            pass
        return r, float(reward) / finished if finished != 0 else 0

def read_play_back_file(fname):
    with open(fname, "rb") as f:
        data = pickle.load(f)

    workload = data["workload"]
    actions = data["result"]["path"]
    cost = data["result"]["path"]
    #for w in workload:
    #    print(w)
    #print(data["result"])
    return workload, actions, cost


def sample_norm(sigma_ratio, miu_ratio, min_val, max_val):
    val = np.random.randn()
    n = (sigma_ratio * max_val) * val + (miu_ratio * max_val)
    n = int(n)
    if n < min_val:
        n = min_val
    if n > max_val:
        n = max_val
    #print("val", val, "sigma_ratio", sigma_ratio, "miu_ratio", miu_ratio, "min", min_val, "max", max_val, "sigma", sigma_ratio * max_val, "miu", miu_ratio * max_val)
    return n


def random_workload_distribution(n, cluster):
    workload = []
    num_nodes = len(cluster.nodes)
    node = cluster.nodes[0]
    max_cpu = node.cpu
    max_mem = node.mem
    max_disk = node.disk
    max_duration = min(n, 50)
    expected_finish = int(n / num_nodes * max_duration / 2)

    ratio_min = 0.2
    ratio_max = 0.5

    #max_start_time = int(float(n) / num_nodes * max_duration / 8)
    max_start_time = max(10, expected_finish - max_duration) / 2
    ratios = [ratio_min, ratio_min, ratio_min]

    #print(n, num_nodes, max_start_time, max_duration)
    # 2 / 8 split
    sigma = 1

    for i in range(n):
        major = np.random.randint(0, 3)
        ratios[major] = ratio_max
        #print('ratios', ratios)
        #print('major', major)

        cpu = sample_norm(ratio_min/2, ratios[0], 1, max_cpu)
        #print('cpu', cpu)
        mem = sample_norm(ratio_min/2, ratios[1], 1, max_mem)
        #print('mem', mem)
        disk = sample_norm(ratio_min/2, ratios[2], 1, max_disk)
        #print('disk', disk)
        #sigma = max_duration/2
        duration = sample_norm(0.5, 0.5, 1, max_duration)
        #np.random.randn()
        #duration = np.random.randint(1, max_duration)
        start = np.random.randint(1, max_start_time)
        task = Task(duration, start, cpu, mem, disk)
        workload.append(task)
        ratios[major] = ratio_min

    workload = sorted(workload, key=lambda x: x.start)
    sum_cpu = 0
    sum_mem = 0
    sum_disk = 0
    disks = []
    for w in workload:
        sum_cpu += w.cpu
        sum_mem += w.mem
        sum_disk += w.disk
        disks.append(w.disk)
    #print("mean cpu", sum_cpu / n)
    #print('mean mem', sum_mem / n)
    #print('mean disk', sum_disk / n)
    #import traceback
    #traceback.print_stack()
    #print('disks', disks)


    return workload


def random_workload(n, cluster, use_fixed="../exp-4-14.pkl"):
    workload = []
    if use_fixed is not None:
        workload, _, _ = read_play_back_file(use_fixed)
        return workload

    node = cluster.nodes[0]
    for i in range(n):
        # mu = 0, sigma=1, 3 samples
        cpu = np.random.randint(1, node.cpu)
        mem = np.random.randint(1, node.mem)
        disk = np.random.randint(1, node.disk)
        duration = np.random.randint(1, 10)
        start = np.random.randint(1, 10)
        task = Task(duration, start, cpu, mem, disk)
        workload.append(task)

    workload = sorted(workload, key=lambda x: x.start)
    return workload



class GymEnvironment(gym.Env):
    def __init__(self, nodes=4, num_tasks=100, cpu=16, mem=64, disk=1024, workload=None):
        self.nodes = nodes
        self.num_tasks = num_tasks
        self.cpu = cpu
        self.mem = mem
        self.disk = disk
        self.task = None
        self.cluster = None
        self.workload = None
        self.action_space = spaces.Discrete(nodes+1)
        # TODO(jiale) what max should this be
        self.observation_space = spaces.Box(0, 10000, shape=(3+3*nodes,), dtype=np.int32)
        self.reset()

    def render(self):
        print("-------------------------")
        cpus = []
        mems = []
        disks = []
        for i in range(self.nodes):
            cpus.append(self.cluster.nodes[i].cpu)
            mems.append(self.cluster.nodes[i].mem)
            disks.append(self.cluster.nodes[i].disk)
        print('clock', self.cluster.clock)
        print("task", self.task)
        print("node\t", ''.join(["{}\t".format(x) for x in range(self.nodes)]))
        print("cpu\t", ''.join(["{}\t".format(x) for x in cpus]))
        print("mem\t", ''.join(["{}\t".format(x) for x in mems]))
        print("disk\t", ''.join(["{}\t".format(x) for x in disks]))
        print("running\t", self.cluster.running_queue)

    def get_state(self):
        n = 3 + 3 * self.nodes + 1
        s = np.zeros(n, dtype=np.int32)
        task = self.task
        if task is not None:
            s[0] = task.cpu
            s[1] = task.mem
            s[2] = task.disk
        nodes = self.nodes
        cluster_nodes = self.cluster.nodes
        for i in range(nodes):
            s[3 + i] = cluster_nodes[i].cpu
        for i in range(nodes):
            s[3 + nodes + i] = cluster_nodes[i].mem
        for i in range(nodes):
            s[3 + 2*nodes + i] = cluster_nodes[i].disk
        s[-1] = self.cluster.clock

        return s

    def step(self, action):
        # state -> (cpu, mem, disk)[(cpu, cpu, cpu),(mem,mem,mem),(disk,disk,disk)]
        # action -> which node to run current task
        #print('step with action', action)
        #import traceback
        #traceback.print_stack()
        reward = 0
        if action == self.nodes:
            # no sheduling
            #print("run task with nop action")
            return self.get_state(), -1, False, {}

        terminal = False

        # actually run task. Get normalized reward
        placed, reward = self.cluster.run(self.task, action)

        if placed is False:
            # send for replacement
            if len(self.workload) != 0:
                self.task = self.workload.pop(0)
            return self.get_state(), -1, terminal, {}

        # set new self.task
        if len(self.workload) == 0:
            if self.task is None:
                terminal = True
            else:
                self.task = None
        else:
            self.task = self.workload.pop(0)

        return self.get_state(), reward - 1, terminal, {}

    def reset(self):
        self.cluster = Cluster(num_nodes=self.nodes, cpu=self.cpu, mem=self.mem, disk=self.disk)
        if self.fixed_workload is None:
            #print("random workload")
            #self.workload = random_workload(4, self.cluster)
            self.workload = random_workload_distribution(self.num_tasks, self.cluster)
        else:
            #print("fixed workload")
            self.workload = copy.deepcopy(self.fixed_workload)
        #print(self.workload)
        self.task = self.workload.pop(0)
        return self.get_state()

class GymEnvironmentTimeReward(GymEnvironment):
    def __init__(self, nodes=4, num_tasks=100, cpu=16, mem=64, disk=1024, workload=None):
        self.fixed_workload = workload
        super(GymEnvironmentTimeReward, self).__init__(nodes=nodes, num_tasks=num_tasks, cpu=cpu, mem=mem, disk=disk, workload=workload)
        self.workload = copy.deepcopy(workload)
        self.action_space = spaces.Discrete(nodes)
        # TODO(jiale) what max should this be
        self.observation_space = spaces.Box(0, 10000, shape=(3+3*nodes+1,), dtype=np.int32)
        self.reset()

    def step(self, action):
        if self.task is None:
            # no more tasks to run
            t = 0
            while len(self.cluster.running_queue) != 0:
                t += self.cluster.tick()
            reward = -t
            return self.get_state(), reward, True, {}

        duration = self.cluster.schedule(self.task, action)
        reward = -duration

        if len(self.workload) == 0:
            self.task = None
        else:
            self.task = self.workload.pop(0)

        return self.get_state(), reward, False, {}


def main():
    pass


if __name__ == '__main__':
    main()
