def numpy():
    return dict(
            lr=1e-4,
            max_grad_norm=40,
            nsteps=100,
    )
