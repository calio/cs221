#import util
from simulator import *
import pickle
import copy

'''
class SchedulingProblem(util.SearchProblem):
    def __init__(self, cluster, workload):
        self.cluster = cluster
        self.workload = workload
        # number of machines
        self.N = len(self.cluster.nodes)
        self.T = len(workload)

    def startState(self):
        machine = self.cluster.nodes[0]
        cpu = machine.total_cpu
        mem = machine.total_mem
        disk = machine.total_disk

        return (self.T, ((cpu, mem, disk) for i in range(self.N)))

    # Return whether |state| is an end state or not.
    def isEnd(self, state):
        state[0] == self.T

    # Return a list of (action, newState, cost) tuples corresponding to edges
    # coming out of |state|.
    def succAndCost(self, state):
        if state[0] == self.T:
            # no more tasks to schedule
            return []

        res = []
        t = state[0] + 1
        work = self.workload[t]
        for i in self.N:
            m = list(state[1][i])
            m.cpu -= work.cpu
            m.mem -= work.mem
            m.disk -= work.disk
            if m.cpu <= 0 or m.mem <= 0 or m.disk <= 0:
                # not enough resource on this machine
                continue
            r = list(state[1])[i] = tuple(m)
            res.append((i, (t, tuple(r), ), ))

        return res
'''


def search(cluster, workload, path, cost, result):
#    print("------ search -----")
#    print("cluster", cluster)
#    print("workload", workload)
#    print("path", path)
#    print("cost", cost)
#    print("result", result)

    if len(workload) == 0:
        # finished all tasks
        t = 0
        while len(cluster.running_queue) != 0:
            t += cluster.tick()
        cost += t

        #print("final cost", cost, "path", path)
        if cost < result["cost"]:
            result["cost"] = cost
            result["path"] = path
        return

    N = len(cluster.nodes)
    for i in range(N):
        # save state
        task = workload.pop(0)
        task_copy = copy.deepcopy(task)
        c = copy.deepcopy(cluster)
        duration = c.schedule(task_copy, i)
        search(c, workload, path + [i], cost + duration, result)
        # resover state
        workload.insert(0, task)

        # make a copy of cluster and worklaod

def search_best_path(orig_cluster, workload):
    result = {"cost":float("inf"), "path":[]}
    cluster = copy.deepcopy(orig_cluster)

    search(cluster, workload, [], 0, result)
    return result


def main():
    cluster_size = 4
    workload_size = 17

    c = Cluster(cluster_size)
    workload = random_workload(workload_size, c)
    print("random workload", workload)

    res = search_best_path(c, workload)
    print("result", res)

    with open("exp-{}-{}.pkl".format(cluster_size, workload_size), "wb") as f:
        pickle.dump({"workload": workload, "result": res}, f)


if __name__ == '__main__':
    main()
