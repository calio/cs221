import pickle
import copy
import simulator as sim

if __name__ == '__main__':
    with open("exp-4-14.pkl", "rb") as f:
        data = pickle.load(f)

    workload = data["workload"]
    actions = data["result"]["path"]
    for w in workload:
        print(w)
    print(data["result"])
    env = sim.GymEnvironmentTimeReward(nodes=4, workload=workload)
    state = env.reset()
    rewards = []
    i = 0
    n = len(actions)
    while True:
    #for _ in range(1000):
        env.render()
        action = actions[i % n]
        print('action', action)
        state, reward, done, _ = env.step(action) # take a random action
        print("reward", reward, "done", done)
        rewards.append(reward)
        i += 1
        if done:
            break
    env.close()
    print("total reward", sum(rewards))
    print("rewards", rewards)

